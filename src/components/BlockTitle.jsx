import React from 'react';

import './BlockTitle.less';

const BlockTitle = ({
  title,
  children,
}) => {
  return (
    <div className="block-title block-title">
      {title && (<span>{title}</span>)}
      {children}
    </div>
  );
};

export default BlockTitle;
