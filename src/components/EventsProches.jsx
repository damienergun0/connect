import React from 'react';
import { Link } from 'framework7-react';

import './EventsProches.less';

const EventsProches = ({ events, useIcon, backText }) => {
  return (
    <div className="block events">
      {events.map((event) => (
        <div className="event" key={event.id}>
          <Link noLinkClass href={`/event/${event.id}`} routeProps={{ backText }}>
            <div className="event-headline" >{event.featured}</div>
            <div className="event-title">{event.title}</div>
            <div className="event-subtitle">{event.subtitle}</div>
            <img loading="lazy" className="event-image" alt={event.title} src={useIcon ? event.icon : event.screenshots[0]} />
          </Link>
        </div>
     ))}
    </div>
  );
};

export default EventsProches;
