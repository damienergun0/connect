import React from 'react';
import { Page, Navbar } from 'framework7-react';
import PageTitle from './PageTitle';
import './PageCustom.less';

const PageCustom = ({
  title, navbarHeading, noCollapsedNavbar, children,
}) => {
  return (
    <Page className={`page ${noCollapsedNavbar ? 'page-no-collapsed-navbar' : ''}`}>
      <Navbar className={`page-large-navbar ${noCollapsedNavbar ? 'page-no-collapsed-navbar' : ''}`} large transparent title={title} backLink="Back"/>
      {title && (
        <PageTitle
          title={title}
          heading={navbarHeading}
        />
      )}
      {children}
    </Page>
  );
};

export default PageCustom;
