import React from 'react';
import { Link } from 'framework7-react';
import avatarSrc from '../assets/avatar.png';
import './PageTitle.less';

const PageTitle = ({
  title,
  heading,
  accountLink = true,
}) => {
  return (
    <h1 className="page-title">
      {heading && (
        <div className="page-title-heading">{heading}</div>
      )}
      <span>{title}</span>
      {accountLink && (
        <Link href="/account/">
          <img src={avatarSrc} alt="Account" />
        </Link>
      )}
    </h1>
  );
};

export default PageTitle;
