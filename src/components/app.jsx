import React, { useState, useEffect } from 'react';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import { auth } from "../js/firebase-config";
import { getDevice }  from '../js/framework7-custom.js';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter,
  Col,
  Row, 
  Button,
} from 'framework7-react';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import store from '../js/store';
import { async } from '@firebase/util';

function MyApp() {
  // Login screen demo data
  // const [username, setUsername] = useState('');
  // const [password, setPassword] = useState('');
 
 
  // login logout register

  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const device = getDevice();
  // const [user, setUser] = useState({});
  
  // onAuthStateChanged(auth, (currentUser) => {
  //   setUser(currentUser);
  // });

  const register = async () => {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      );
    } catch (error) {
      console.log(error.message);
    }
    f7.loginScreen.close();
  };

  const login = async () => {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
    } catch (error) {
      console.log(error.message);
    }
    f7.loginScreen.close();
  };


  const logout = async () => {
    await auth.signOut();
  
  };
  
  // listen for auth status changing
    auth.onAuthStateChanged(user => {
    if (user) {
      console.log('user logged in :' , user)
    } else {
      console.log('user logged out')
    }
  } ) ; 

  
 
  // Framework7 Parameters

  const f7params = {
    name: 'connect', // App name
      theme: 'auto', // Automatic theme detection


      id: 'io.framework7.myapp', // App bundle ID
      // App store
      store: store,
      // App routes
      routes: routes,

      // Input settings
      input: {
        scrollIntoViewOnFocus: device.capacitor,
        scrollIntoViewCentered: device.capacitor,
      },
      // Capacitor Statusbar settings
      statusbar: {
        iosOverlaysWebView: true,
        androidOverlaysWebView: false,
      },
  };


 

  const closeloginscreen = () => {
      f7.loginScreen.close();
  }
  f7ready(() => {

    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });


  return (
    <App { ...f7params } >

        {/* Left panel with cover effect*/}
        <Panel left cover dark>
          <View>
            <Page>
              <Navbar title="Left Panel"/>
              <Block>Left panel content goes here

              </Block>
            </Page>
          </View>
        </Panel>


        {/* Right panel with reveal effect*/}
        <Panel right reveal dark>
          <View>
            <Page>
              <Navbar title="Right Panel"/>
              <Block>Right panel content goes here</Block>
            </Page>
          </View>
        </Panel>


        {/* Views/Tabs container */}
        
        <Views tabs className="safe-areas">
          {/* Tabbar for switching views-tabs */}
          <Toolbar tabbar labels bottom>
            <Link tabLink="#view-home" tabLinkActive iconIos="f7:house_fill" iconAurora="f7:house_fill" iconMd="material:home" text="Home" />
            {/* <Link tabLink="#view-catalog" iconIos="f7:square_list_fill" iconAurora="f7:square_list_fill" iconMd="material:view_list" text="Catalog" /> */}
            <Link tabLink="#view-events" iconIos="f7:square_list_fill" iconAurora="f7:square_list_fill" iconMd="material:view_list" text="Événements" />
            {/* <Link tabLink="#view-settings" iconIos="f7:gear" iconAurora="f7:gear" iconMd="material:settings" text="Settings" /> */}
          </Toolbar>

          {/* Your main view/tab, should have "view-main" class. It also has "tabActive" prop */}
          <View id="view-home" main tab tabActive url="/" />

          {/* Catalog View
          <View id="view-catalog" name="catalog" tab url="/catalog/" /> */}

          {/* Events View */}
          <View id="view-events" name="events"  tab url="/events/" />
          
          {/* Settings View */}
          <View id="view-settings" name="settings" tab url="/settings/" />

        </Views>

      {/* Popup */}
      <Popup id="my-popup">
        <View>
          <Page>
            <Navbar title="Popup">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <Block>
              <p>Popup content goes here.</p>
            </Block>
          </Page>
        </View>
      </Popup>

      <LoginScreen id="my-signin-screen">
        <View>
          <Page loginScreen >
            <NavRight>
            <ListButton title = "go back"  onClick={closeloginscreen}/>  
            </NavRight>


            <LoginScreenTitle>create a new account</LoginScreenTitle>
            <List form id = "RegisterForm">
              <ListInput
                type="text"
                name="email"
                placeholder="Your email"
                value={registerEmail}
                onInput={(e) =>setRegisterEmail(e.target.value)}
              ></ListInput>
              <ListInput
                type="password"
                name="password"
                placeholder="Your password"
                value={registerPassword}
                onChange={(e) => setRegisterPassword(e.target.value)}
              ></ListInput>
            </List>
            <List>
              <ListButton title="Create an account" onClick={register}      />
              <ListButton title = "go back" onClick={closeloginscreen}/>  

              <BlockFooter>
                Some text about login information.<br />Click "Sign In" to close Login Screen
              </BlockFooter>
            </List>
          </Page>
        </View>
      </LoginScreen>

      <LoginScreen id="my-login-screen">
        <View>
          <Page loginScreen>
            <NavRight>
            <ListButton title = "go back"  onClick={closeloginscreen}/>  
            </NavRight>
            <LoginScreenTitle>Log in</LoginScreenTitle>
            <List form id = "LoginForm">
              <ListInput
                type="text"
                name="email"
                placeholder="Your email"
                value={loginEmail}
                onInput={(e) => setLoginEmail(e.target.value)}
              ></ListInput>
              <ListInput
                type="password"
                name="password"
                placeholder="Your password"
                value={loginPassword}
                onChange={(e) => setLoginPassword(e.target.value)}
              ></ListInput>
            </List>
            <List>
              <ListButton title="Log in" onClick={login}/>
              <BlockFooter>
                Some text about login information.<br />Click "Sign In" to close Login Screen
              </BlockFooter>
            </List>
          </Page>
        </View>
      </LoginScreen>
      
      <Popup id="my-logout-popup">
        <View>
          <Page>
            <Navbar title="log out">
              <NavRight>

              </NavRight>
            </Navbar>
            <Block >
              <p algn = "center">Popup content goes here.</p>
              <Row>
                <Col width="50">
                  <Button popupClose> No </Button>
                </Col>
                <Col width="50">
                  <Button popupClose onClick={logout}> Yes </Button>
                </Col>
              </Row>
            </Block>
          </Page>
        </View>
      </Popup>


      {/* <LoginScreen id="my-login-screen">
        <View>
          <Page loginScreen>
            <LoginScreenTitle>Login</LoginScreenTitle>
            <List form>
              <ListInput
                type="text"
                name="username"
                placeholder="Your username"
                value={username}
                onInput={(e) => setUsername(e.target.value)}
              ></ListInput>
              <ListInput
                type="password"
                name="password"
                placeholder="Your password"
                value={loginPassword}
                onInput={(e) => setLoginPassword(e.target.value)}
              ></ListInput>
            </List>
            <List>
              <ListButton title="Sign In" onClick={() => alertLoginData()} />
              <BlockFooter>
                Some text about login information.<br />Click "Sign In" to close Login Screen
              </BlockFooter>
            </List>
          </Page>
        </View>
      </LoginScreen> */}

    </App>
  )
}
export default MyApp;
