/* eslint-disable */
//https://stackoverflow.com/questions/44980247/how-to-finish-all-fetch-before-executing-next-function-in-react
import { Geolocation } from '@capacitor/geolocation';

var pos;
var events;
var eventsprochesliste = [];

function getEvents() {
  return fetch('../data/events.json')
    .then(res => res.json())
    .then(data => events = data)
};

const CurrentPosition = async () => {
  const coordinates = await Geolocation.getCurrentPosition();
  return coordinates
};


function getEventsAndPos() {
  return Promise.all([getEvents(), CurrentPosition()])
}

// Request both students and scores in parallel and return a Promise for both values.
// `Promise.all` returns a new Promise that resolves when all of its arguments resolve.

// When this Promise resolves, both values will be available.
function getEventsProches() {
  getEventsAndPos().then(([events,pos]) => {
    eventsprochesliste = []
    events.forEach((event) => {
      var dist = 6371 * Math.acos((Math.sin(pos['coords']['latitude']) * Math.sin(event.lat) + Math.cos(pos['coords']['latitude']) * Math.cos(event.lat) * Math.cos(event.long - pos['coords']['longitude'])))
      if (dist < 600) {
        eventsprochesliste.push(event);
      };
      if (event.icon) event.icon = `static/apps-images/${event.icon}`;
      event.screenshots = event.screenshots.map((fileName) => `static/apps-images/${fileName}`);
      event.screenshotsThumbs = event.screenshots.map((fileName) => fileName.replace('.jpg', '-thumb.jpg'));
    }
    )
  }
  );
  return eventsprochesliste
}

eventsprochesliste = getEventsProches()
export { events, eventsprochesliste, getEventsProches }
