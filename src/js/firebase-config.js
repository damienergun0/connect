import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "@firebase/firestore" ; 

const firebaseConfig = {
  apiKey: "AIzaSyCc39PejqJu-eeIm6YKZB8zvThWr9QpXMY",
  authDomain: "connect-app-e7299.firebaseapp.com",
  projectId: "connect-app-e7299",
  storageBucket: "connect-app-e7299.appspot.com",
  messagingSenderId: "775777299951",
  appId: "1:775777299951:web:6dac6d53df9214ae1d1a1a",
  measurementId: "G-W1Q05QX4ZR"
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getFirestore(app);

