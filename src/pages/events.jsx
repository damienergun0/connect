import React from 'react';
import {
  Block,
  Button,
  Link,
  List,
  ListItem,
} from 'framework7-react';
import { Page, Navbar} from 'framework7-react';

const Events = () => {
  return (
    <Page title="Événements">
      <Navbar large transparent title="Événements"/>
      
      <List>
      <ListItem
        title="Rejoindre un événement"
        link="/eventsjoin/"
      />
      <ListItem
        title="Créér un événement"
        link="/eventscreer/"
      />
    </List>
    </Page>)
};

export default Events;
