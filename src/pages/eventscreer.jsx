import React, {useState} from 'react';
import {
  Button,
  List,
  ListInput,
  BlockTitle,
} from 'framework7-react';

import PageCustom from '../components/PageCustom';
import { Geolocation } from '@capacitor/geolocation';

function sendForm(data) {
  console.log(data)}

const Eventscreer = () => {
  
  const CurrentPosition = async () => {
    const coordinates = await Geolocation.getCurrentPosition();
    return coordinates
  };

  var coords = CurrentPosition()
  return (
    
    <PageCustom title="Événements" >
      <List form onSubmit={(data) => sendForm(data)} noHairlinesMd>
      <BlockTitle>Formulaire de contact</BlockTitle>
      <ListInput clearButton
        label="Titre"
        type="text"
        placeholder="Titre"
        required
      ></ListInput>
      <ListInput
        label="Description courte"
        type="text"
        placeholder="Description courte"
        required
      ></ListInput>
      <ListInput
        label="Identifiant unique"
        type="text"
        placeholder="mon-evenement-cool"
        required
        //ajouter validation d'unicité -> appel BDD plus tard 
      ></ListInput>
      <ListInput
        label="Nombre d'invités"
        type="int"
        placeholder=""
        required
      ></ListInput>
      <ListInput timePicker
        label="Date de début"
        type="datetime-local"
        required
      ></ListInput>
      <ListInput timePicker 
        label="Date de fin"
        type="datetime-local"
        required
      ></ListInput>
      <ListInput
        label="Événement privé"
        type="checkbox"
      ></ListInput>
      <ListInput
        label="Notifications si quelqu'un rejoint"
        type="checkbox"
      ></ListInput>
      <Button type="submit" className="col" round fill>
          Créer l'événement 
        </Button>
      
    </List>
    </PageCustom>)
    
  };

export default Eventscreer;
