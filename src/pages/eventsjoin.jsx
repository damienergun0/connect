import React, {componentDidMount} from 'react';
import {
  Block,
  Button,
  Link,
  List,
  ListItem,
} from 'framework7-react';

import PageCustom from '../components/PageCustom';
import {getEventsProches,eventsprochesliste} from '../js/data';
import EventsProches from '../components/EventsProches';

const Eventsjoin = () => {
  const [listeeventsproches, setEventsProches] = React.useState(
    eventsprochesliste);
  const Update = () => {setEventsProches(getEventsProches())}
  React.useEffect(() => { Update() }, [])
  return (
    
    <PageCustom title="Événements">
      <EventsProches backText="Événements proches" events={listeeventsproches} />
    <Button href = '/eventsjoin/' onClick = {Update}>Refresh</Button>
    </PageCustom>)
};

export default Eventsjoin;
