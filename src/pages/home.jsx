import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button
} from 'framework7-react';
import { auth } from "../js/firebase-config";
import MyApp from '../components/app';



const HomePage = () => {



  return (
  <Page name="home">
    {/* Top Navbar */}
    <Navbar  sliding={false}  >
      <NavLeft>
        <Link iconIos="f7:menu" iconAurora="f7:menu" iconMd="material:menu" panelOpen="left" />
      </NavLeft>
      <NavTitle sliding  >Connect</NavTitle>
      <NavRight>
        <Link iconIos="f7:menu" iconAurora="f7:menu" iconMd="material:menu" panelOpen="right" />
      </NavRight>
    </Navbar>
   
    {/* Page content */}
        <Block strong>
          <Button fill raised popupOpen="#my-logout-popup">log out</Button>
        </Block>

  </Page>) ;

} ;
export default HomePage;