import React from 'react';
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button
} from 'framework7-react';

const Start = () => {
    return(
    <Page name="start">
      {/* Top Navbar */}
            {/* Top Navbar */}
            <Navbar  sliding={false} noShadow={false} >
            <NavTitle sliding  >Connect</NavTitle>
          </Navbar>
      {/* Page content */}
        <Row>
          <Col width="50">
            {/* <Button fill raised popupOpen="#my-popup">Popup</Button> */}
            <Button fill raised loginScreenOpen="#my-login-screen">Login Screen</Button>  
          </Col>
          <Col width="50">
            <Button fill raised loginScreenOpen="#my-signin-screen">Register Screen</Button>  
          </Col>
        </Row>
        </Page>)
  };
  export default Start;